from __future__ import division
import warnings
import numpy as np
import scipy as sp
from matplotlib import image
from random import randint
from scipy.misc import logsumexp
from helper_functions import image_to_matrix, matrix_to_image, \
                             flatten_image_matrix, unflatten_image_matrix, \
                             image_difference

warnings.simplefilter(action="ignore", category=FutureWarning)


def k_means_cluster(image_values, k=3, initial_means=None):
    """
    Separate the provided RGB values into
    k separate clusters using the k-means algorithm,
    then return an updated version of the image
    with the original values replaced with
    the corresponding cluster values.

    params:
    image_values = numpy.ndarray[numpy.ndarray[numpy.ndarray[float]]]
    k = int
    initial_means = numpy.ndarray[numpy.ndarray[float]] or None

    returns:
    updated_image_values = numpy.ndarray[numpy.ndarray[numpy.ndarray[float]]]
    """
    k_means = initial_means.copy()
    flat_image = flatten_image_matrix(image_values)

    # If no initial clusters then choose K random points from the data
    if k_means is None:
        num_pixels = flat_image.shape[0]
        random_indices = np.random.choice(num_pixels, k, replace=False)
        k_means = flat_image[random_indices]

    current_sse = 1e10
    previous_sse = 1e1

    while not np.isclose(np.abs(previous_sse - current_sse), 0.0):
        # SSE of each pixel to its assigned mean
        k_sse = []
        for k_i in range(k):
            k_sse.append(np.sum((np.square(flat_image - k_means[k_i]))
                                        , axis=1))
        k_sse = np.array(k_sse).T

        # Assign each pixel to its closest cluster
        clusters = np.argmin(k_sse, axis=1)

        for k_i in range(k):
            cluster_k_indices = np.where(clusters == k_i)
            k_means[k_i] = np.mean(flat_image[cluster_k_indices], axis=0)

        previous_sse, current_sse = current_sse, np.sum(np.min(k_sse, axis=1))

    updated_image_values = unflatten_image_matrix(k_means[clusters], image_values.shape[1])

    return updated_image_values


def default_convergence(prev_likelihood, new_likelihood, conv_ctr,
                        conv_ctr_cap=10):
    """
    Default condition for increasing
    convergence counter:
    new likelihood deviates less than 10%
    from previous likelihood.

    params:
    prev_likelihood = float
    new_likelihood = float
    conv_ctr = int
    conv_ctr_cap = int

    returns:
    conv_ctr = int
    converged = boolean
    """
    increase_convergence_ctr = (abs(prev_likelihood) * 0.9 <
                                abs(new_likelihood) <
                                abs(prev_likelihood) * 1.1)

    if increase_convergence_ctr:
        conv_ctr += 1
    else:
        conv_ctr = 0

    return conv_ctr, conv_ctr > conv_ctr_cap


class GaussianMixtureModel:
    """
    A Gaussian mixture model
    to represent a provided
    grayscale image.
    """

    def __init__(self, image_matrix, num_components, means=None):
        """
        Initialize a Gaussian mixture model.

        params:
        image_matrix = (grayscale) numpy.nparray[numpy.nparray[float]]
        num_components = int
        """
        self.image_matrix = image_matrix
        self.num_components = num_components
        if(means is None):
            self.means = np.zeros(num_components)
        else:
            self.means = means
        self.variances = np.zeros(num_components)
        self.mixing_coefficients = np.zeros(num_components)

    def joint_prob(self, val):
        """Calculate the joint
        log probability of a greyscale
        value within the image.

        params:
        val = float

        returns:
        joint_prob = float
        """
        # Calculate:
        # ln(mixing_k * N(x_n | mean_k,stdev_k))
        # and account for numerical underflow by doing a logsumexp
        log_probabilities = (-0.5 * np.log(2 * np.pi * self.variances)) - \
                    (((val - self.means)**2) / (2 * self.variances))
        return logsumexp(a=log_probabilities,b=self.mixing_coefficients)

    def initialize_training(self):
        """
        Initialize the training
        process by setting each
        component mean to a random
        pixel's value (without replacement),
        each component variance to 1, and
        each component mixing coefficient
        to a uniform value
        (e.g. 4 components -> [0.25,0.25,0.25,0.25]).

        NOTE: this should be called before
        train_model() in order for tests
        to execute correctly.
        """
        flat_image = flatten_image_matrix(self.image_matrix)
        self.means = np.random.choice(flat_image.squeeze(),
                                      self.num_components, replace=False)
        # Set each component variance to 1
        self.variances = np.ones(self.num_components)
        # Set each mixing coefficient to a uniform value
        self.mixing_coefficients = np.ones(self.num_components) / \
                                    self.num_components

    def train_model(self, convergence_function=default_convergence):
        """
        Train the mixture model
        using the expectation-maximization
        algorithm. Since each Gaussian is
        a combination of mean and variance,
        this will fill self.means and
        self.variances, plus
        self.mixing_coefficients, with
        the values that maximize
        the overall model likelihood.

        params:
        convergence_function = function, returns True if convergence is reached
        """
        converged = False
        conv_ctr = 0
        prev_likelihood = self.likelihood()
        flat_image = flatten_image_matrix(self.image_matrix)

        while not converged:
            # Calculate Responsibilities using current parameter values
            log_probabilities = (-0.5 * np.log(2 * np.pi * self.variances)) - \
                    (((flat_image - self.means)**2) / (2 * self.variances))

            numerator = self.mixing_coefficients * np.exp(log_probabilities)
            denominator = np.sum(numerator, axis=1)
            gamma = numerator/denominator.reshape(denominator.shape[0], 1)

            N_k = np.sum(gamma, axis=0)

            # Re-estimate mean
            self.means = np.sum(gamma * flat_image, axis=0) / N_k
            # Re-estimate Covariance
            self.variances = np.sum(gamma * np.square(flat_image - self.means)
                                    , axis=0) / N_k
            # Re-estimate Mixture Coefficients
            self.mixing_coefficients = N_k / flat_image.shape[0]

            current_likelihood = self.likelihood()
            conv_ctr, converged = convergence_function(prev_likelihood, current_likelihood, conv_ctr)
            prev_likelihood = current_likelihood

    def segment(self):
        """
        Using the trained model,
        segment the image matrix into
        the pre-specified number of
        components. Returns the original
        image matrix with the each
        pixel's intensity replaced
        with its max-likelihood
        component mean.

        returns:
        segment = numpy.ndarray[numpy.ndarray[float]]
        """
        flat_image = flatten_image_matrix(self.image_matrix)

        # Calculate Responsibilities using current parameter values
        log_probabilities = (-0.5 * np.log(2 * np.pi * self.variances)) - \
                (((flat_image - self.means)**2) / (2 * self.variances))

        numerator = self.mixing_coefficients * np.exp(log_probabilities)
        denominator = np.sum(numerator, axis=1)
        gamma = numerator / denominator.reshape(denominator.shape[0], 1)

        best_component = np.argmax(gamma, axis=1)

        # Replace each pixel intensity with its max-likelihood component mean
        segment = self.means[best_component.reshape(flat_image.shape)]

        segment_image = segment.reshape(self.image_matrix.shape)

        return segment_image

    def likelihood(self):
        """Assign a log
        likelihood to the trained
        model based on the following
        formula for posterior probability:
        ln(Pr(X | mixing, mean, stdev)) = sum((n=1 to N), ln(sum((k=1 to K),
                                          mixing_k * N(x_n | mean_k,stdev_k))))

        returns:
        log_likelihood = float [0,1]
        """
        flat_image = flatten_image_matrix(self.image_matrix)
        log_probabilities = (-0.5 * np.log(2 * np.pi * self.variances)) - \
                (((flat_image - self.means)**2) / (2 * self.variances))
        log_likelihood = logsumexp(a=log_probabilities,
                                  b=self.mixing_coefficients, axis=1)
        return np.sum(log_likelihood)

    def best_segment(self, iters):
        """Determine the best segmentation
        of the image by repeatedly
        training the model and
        calculating its likelihood.
        Return the segment with the
        highest likelihood.

        params:
        iters = int

        returns:
        segment = numpy.ndarray[numpy.ndarray[float]]
        """
        best_segment = None
        max_likelihood = float('-inf')

        for _ in range(iters):
            self.initialize_training()
            self.train_model()
            current_likelihood = self.likelihood()
            if current_likelihood > max_likelihood:
                max_likelihood = current_likelihood
                best_segment = self.segment()

        return best_segment


class GaussianMixtureModelImproved(GaussianMixtureModel):
    """A Gaussian mixture model
    for a provided grayscale image,
    with improved training
    performance."""

    def initialize_training(self):
        """
        Initialize the training
        process by setting each
        component mean using some algorithm that
        you think might give better means to start with,
        each component variance to 1, and
        each component mixing coefficient
        to a uniform value
        (e.g. 4 components -> [0.25,0.25,0.25,0.25]).
        [You can feel free to modify the variance and mixing coefficient
         initializations too if that works well.]
        """
        flat_image = flatten_image_matrix(self.image_matrix)
        k_means = np.random.choice(flat_image.squeeze(),
                                      self.num_components, replace=False)
        # Set each component variance to 1
        self.variances = np.ones(self.num_components)
        # Set each mixing coefficient to a uniform value
        self.mixing_coefficients = np.ones(self.num_components) / \
                                    self.num_components

        current_sse = 1e10
        previous_sse = 1e1

        while not np.isclose(np.abs(previous_sse - current_sse), 0.0):
            # SSE of each pixel to its assigned mean
            k_sse = []
            for k_i in range(k_means.shape[0]):
                k_sse.append(np.sum((np.square(flat_image - k_means[k_i]))
                                            , axis=1))
            k_sse = np.array(k_sse).T

            # Assign each pixel to its closest cluster
            clusters = np.argmin(k_sse, axis=1)

            for k_i in range(k_means.shape[0]):
                cluster_k_indices = np.where(clusters == k_i)
                k_means[k_i] = np.mean(flat_image[cluster_k_indices], axis=0)

            previous_sse, current_sse = current_sse, np.sum(np.min(k_sse, axis=1))

        self.means = k_means


def new_convergence_function(previous_variables, new_variables, conv_ctr,
                             conv_ctr_cap=10):
    """
    Convergence function
    based on parameters:
    when all variables vary by
    less than 10% from the previous
    iteration's variables, increase
    the convergence counter.

    params:

    previous_variables = [numpy.ndarray[float]]
                         containing [means, variances, mixing_coefficients]
    new_variables = [numpy.ndarray[float]]
                    containing [means, variances, mixing_coefficients]
    conv_ctr = int
    conv_ctr_cap = int

    return:
    conv_ctr = int
    converged = boolean
    """
    lower_bound = (previous_variables * 0.9) <= new_variables
    upper_bound = new_variables <= (previous_variables * 1.1)

    increase_convergence_ctr = lower_bound.all() and upper_bound.all()

    if increase_convergence_ctr:
        conv_ctr += 1
    else:
        conv_ctr = 0

    return conv_ctr, conv_ctr > conv_ctr_cap


class GaussianMixtureModelConvergence(GaussianMixtureModel):
    """
    Class to test the
    new convergence function
    in the same GMM model as
    before.
    """
    def train_model(self, convergence_function=new_convergence_function):
        converged = False
        conv_ctr = 0
        prev_model_params = np.array([self.means, self.variances, self.mixing_coefficients], copy=True)
        flat_image = flatten_image_matrix(self.image_matrix)

        while not converged:
            # Calculate Responsibilities using current parameter values
            log_probabilities = (-0.5 * np.log(2 * np.pi * self.variances)) - \
                    (((flat_image - self.means)**2) / (2 * self.variances))

            numerator = self.mixing_coefficients * np.exp(log_probabilities)
            denominator = np.sum(numerator, axis=1)
            gamma = numerator/denominator.reshape(denominator.shape[0], 1)

            N_k = np.sum(gamma, axis=0)

            # Re-estimate mean
            self.means = np.sum(gamma * flat_image, axis=0) / N_k
            # Re-estimate Covariance
            self.variances = np.sum(gamma * np.square(flat_image - self.means)
                                    , axis=0) / N_k
            # Re-estimate Mixture Coefficients
            self.mixing_coefficients = N_k / flat_image.shape[0]

            current_model_params = np.array([self.means, self.variances, self.mixing_coefficients], copy=True)
            conv_ctr, converged = convergence_function(prev_model_params, current_model_params, conv_ctr)
            prev_model_params = current_model_params.copy()


def bayes_info_criterion(gmm):
    rows, cols = gmm.image_matrix.shape
    # Number of data points
    n = rows * cols
    # Likelihood
    L = gmm.likelihood()
    # Number of parameters estimated by the model
    # In the case of the Gaussian mixture model, this is equal to the number
    # of components times the number of variables per component
    # (mean, variance and mixing coefficient) = 3 x components
    k = 3 * gmm.num_components

    return np.log(n) * k - (2 * L)


def BIC_likelihood_model_test():
    """Test to compare the
    models with the lowest BIC
    and the highest likelihood.

    returns:
    min_BIC_model = GaussianMixtureModel
    max_likelihood_model = GaussianMixtureModel

    for testing purposes:
    """
    comp_means = [
        [0.023529412, 0.1254902],
        [0.023529412, 0.1254902, 0.20392157],
        [0.023529412, 0.1254902, 0.20392157, 0.36078432],
        [0.023529412, 0.1254902, 0.20392157, 0.36078432, 0.59215689],
        [0.023529412, 0.1254902, 0.20392157, 0.36078432, 0.59215689,
         0.71372563],
        [0.023529412, 0.1254902, 0.20392157, 0.36078432, 0.59215689,
         0.71372563, 0.964706]
    ]
    max_likelihood = float('-inf')
    max_likelihood_model = None
    lowest_bic = float('inf')
    min_bic_model = None
    image_matrix = image_to_matrix('images/party_spock.png')
    for num_components, i in zip(range(2, 8), range(6)):
        gmm = GaussianMixtureModel(image_matrix, num_components,
                comp_means[i])
        gmm.initialize_training()
        gmm.train_model()
        likelihood = gmm.likelihood()
        bic = bayes_info_criterion(gmm)

        if likelihood > max_likelihood:
            max_likelihood = likelihood
            max_likelihood_model = gmm
        if bic < lowest_bic:
            lowest_bic = bic
            min_bic_model = gmm

    # print "BIC:{} Likelihood:{}".format(min_bic_model.num_components,
    #                                     max_likelihood_model.num_components)
    return min_bic_model, max_likelihood_model


def BIC_likelihood_question():
    """
    Choose the best number of
    components for each metric
    (min BIC and maximum likelihood).

    returns:
    pairs = dict
    """
    # TODO: fill in bic and likelihood
    bic = 7
    likelihood = 7
    pairs = {
        'BIC': bic,
        'likelihood': likelihood
    }
    return pairs

def return_your_name():
    # return your name
    return "Michael Tehranian"

def bonus(points_array, means_array):
    """
    Return the distance from every point in points_array
    to every point in means_array.

    returns:
    dists = numpy array of float
    """
    dists = []
    rows = points_array.shape[0]
    num_sections = 10
    increments = range(0, rows, int(rows/num_sections))
    increments.append(rows)

    for i, j in zip(increments, increments[1:]):
        ssd = np.sqrt(np.sum(np.square(points_array[i:j, np.newaxis]
                    - means_array), axis=2))
        dists.append(ssd)

    return np.vstack(dists)
